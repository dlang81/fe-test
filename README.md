# fe-test

[![Dependency Status](https://david-dm.org/preboot/angularjs-webpack/status.svg)](https://david-dm.org/preboot/angular-webpack#info=dependencies) [![devDependency Status](https://david-dm.org/preboot/angularjs-webpack/dev-status.svg)](https://david-dm.org/preboot/angularjs-webpack#info=devDependencies)

### A frontend developer task

Includes a Dockerfile with Apache Zeppelin and an AngularJS app accessing the notebooks. <br />
Based on [angularjs-webpack](https://github.com/preboot/angularjs-webpack/), which offers features like:

* Heavily commented webpack configuration with reasonable defaults.
* ES6, and ES7 support with babel.
* Source maps included in all builds.
* Development server with live reload.
* Production builds with cache busting.
* Testing environment using karma to run tests and jasmine as the framework.
* Code coverage when tests are run.
* No gulp and no grunt, just npm scripts.

### Quick start

> Clone/Download the repo, then build and run docker image.

```bash
# clone the repo
$ git clone https://gitlab.com/dlang81/fe-test.git

# change directory to project
$ cd fe-test

# build docker image
$ docker build -t fe-test-image .

# run docker image
$ docker run --name fe-test -p 8088:8088 -p 8080:8080 -td fe-test-image
```

go to [http://localhost:8088](http://localhost:8088) in your browser.

### License

[MIT](/LICENSE)
