import app from './app';

describe('app', () => {
  
  let http;
  beforeEach(inject(function($http) {
    http = $http;
  }));

  it('should be true', () => {
    expect(1).toBe(1);
  });

  it('should check if zeppelin api is available', function() {
    http.get('http://localhost:8080/api/notebook/2A94M5J1Z')
    .then(function({data: {status}}) {
      expect(status).toBe("OK")
    });
  });

  describe('AppCtrl', () => {
    let ctrl;

    beforeEach(() => {
      angular.mock.module(app);
      angular.mock.inject(() => {
        //ctrl = $controller('AppCtrl', {});
      });
    });
  });
});