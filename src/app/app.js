import angular from 'angular';
import uiRouter from '@uirouter/angularjs';
import '../style/app.css';

const MOD_NAME = 'app'
const API_HOST = 'http://localhost:8080'
const API_BASE = `${API_HOST}/api`
const NOTE_URL = `${API_BASE}/notebook/2A94M5J1Z`

class AppCtrl {
  
  constructor($http, $window, $state) {
    Object.assign(this, {$http, $window ,$state, status})
    this.$window.onresize = function(){ resizeIframe() }      
    this.pollStatus()
  }
  
  pollStatus() {
    this.$http.get(NOTE_URL)
    .then(function successCallback({data: {body: {paragraphs}}}) {
      for (let [index, paragraph] of paragraphs.entries()) {
        this.status = paragraph.status        
        if (paragraph.status == 'RUNNING'){
          break
        }
      }
    }.bind(this), function errorCallback(response) {
      console.log('Api call failed...')
    })
    setTimeout(this.pollStatus.bind(this), 2000)
  }
}

function resizeIframe() {
  document.getElementById("iframe").style.height = (window.innerHeight - 53) + 'px'
}

angular.module(MOD_NAME, ['ui.router'])
  .controller('AppCtrl', AppCtrl)
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    
    $locationProvider.html5Mode(true)
    $urlRouterProvider.otherwise('/')
    
    $stateProvider
    .state('home', {
      url: '/',
      templateUrl:  'home.html',
      controller: AppCtrl,
      controllerAs: 'vm'
    })
    .state('home.notebook', {
      url: 'notebook',
      templateUrl: 'notebook.html',
      controller: function() {
        resizeIframe()
      }
    })
    .state('home.about', {
      url: 'about',
      templateUrl: 'about.html',
    })
  })

export default MOD_NAME