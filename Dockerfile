FROM apache/zeppelin:0.7.3

WORKDIR /usr/fe-test
COPY ./ /usr/fe-test/
COPY ./zeppelin/zeppelin-site.xml /zeppelin/conf/zeppelin-site.xml
COPY ./zeppelin/interpreter.json /zeppelin/conf/interpreter.json

RUN apt-get update
RUN apt-get install -y curl
RUN curl -sL https://deb.nodesource.com/setup_10.x | bash
RUN apt-get install -y nodejs

EXPOSE 8088
CMD /usr/fe-test/zeppelin/zeppelin-entrypoint.sh
